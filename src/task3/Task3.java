
package task3;
import java.util.Scanner;


public class Task3 {
    
    // Colocamos las variables en static a nivel clase para no tenerlo en ninguna funcion, no reirterar una cantidad inmensa en los argumentos y evitar que se sobreescriban sin querer
    static int sanjose = 0; static int alajuela = 0; static int cartago = 0; static int heredia = 0; static int guanacaste = 0; static int puntarenas = 0; static int limon = 0; // Provincias
    static int juan = 0; static int ana = 0; static int carlos = 0; // Candidatos
    static int[] edadesVotos = new int[3]; // se pondra los rangos de edades dentro un arreglo
        
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int opc;
        
        do {
            System.out.println("\n------ Menu ------\n1. Sets\n2. Votaciones\n3. Salir\nSeleccione ua opcion: ");
            opc = scanner.nextInt();
            
            switch (opc) {
            case 1 -> menu1(scanner);
            case 2 -> menu2(scanner);
            case 3 -> System.out.println("Saliendo..."); 
            default -> System.out.println("Opcion invalida, intente de nuevo.");
        }
        } while (opc != 3);
}
    
    public static void menu1(Scanner scanner) {
        System.out.println("Ingrese el numero de juegos ganados por el jugador A: ");
                int juegosA = scanner.nextInt();
                System.out.println("Ingrese el numero de juegos ganados por el jugador B: ");
                int juegosB = scanner.nextInt();
                System.out.println("Resultado: " + determinarSet(juegosA, juegosB));
    }
    
    public static String determinarSet(int juegosA, int juegosB) {
        if ((juegosA < 0 || juegosA > 7) || (juegosB < 0 || juegosB > 7) || (juegosA == 7 && juegosB == 7) || (juegosA == 7 && juegosB <=4) || (juegosB == 7 && juegosA <=4)) {
            return "El set es invalido.";
        }
        if ((juegosA == 6 && juegosB <= 4) || juegosA == 7) {
            return  "El jugador A gano el set.";
        }
        if ((juegosA == 6 && juegosB <= 4) || juegosA == 7) {
            return  "El jugador A gano el set.";
        }
        return "El set aun no termina.";
    }
    
    
    public static void menu2(Scanner scanner) {
        System.out.println("Ingrese su edad: ");
        int edad = scanner.nextInt();
        if (edad < 18) {
            System.out.println("ERROR: No cumple con la edad minima requerida.");
            return;
        } 
        if (edad >= 18 && edad <= 29) { 
        edadesVotos[0]++;  // indice 0: 18-29 años
        } else if (edad >= 30 && edad <= 59) {
            edadesVotos[1]++; // indice 1: 30-59 años
        } else {
            edadesVotos[2]++; // indice 2: Mayores de 60
        }
        
        
        System.out.println("1- San Jose\n2- Alajuela\n3- Cartago\n4- Heredia\n5- Guanacaste\n6- Puntarenas\n7- Limon\nIngrese su provincia: ");
        int provincia = scanner.nextInt();
        System.out.println("1- Juan\n2- Ana\n3- Carlos");
        int candidato = scanner.nextInt();
        
        switch (provincia) {
            case 1 -> sanjose += 1;
            case 2 -> alajuela += 1;
            case 3 -> cartago += 1;
            case 4 -> heredia += 1;
            case 5 -> guanacaste += 1;
            case 6 -> puntarenas += 1;
            case 7 -> limon += 1;
            default -> System.out.println("");
        }
        switch (candidato) {
            case 1 -> juan++;
            case 2 -> ana++;
            case 3 -> carlos++;
            default -> System.out.println("ERROR: Candidato invalido.");
        }
        
        reporte1();
        reporte2();
        reporte3();
    }
    
    public static void reporte1() {
        System.out.println("\nCandidatos con la cantidad de votos que han recibido: ");
        System.out.println("Juan con " + juan + " votos.");
        System.out.println("Ana con " + ana + " votos.");
        System.out.println("Carlos con " + carlos + " votos.");
    }
    public static void reporte2() {
        int[] votosPorProvincia = {sanjose, alajuela, cartago, heredia, guanacaste, puntarenas, limon}; // Se guarda en array las votaciones por provincia
        // Otro array para guardar los nombres de las provincias y asi usarlo con un for junto con votos por provincia, misma cantidad de iteracion
        String[] provincias = {"San Jose", "Alajuela", "Cartago", "Heredia", "Guanacaste", "Puntarenas", "Limon"};

        ordenarPorVotos(votosPorProvincia, provincias);

        System.out.println("\nCantidad de votos por provincia: ");
        for (int i = 0; i < provincias.length; i++) {
            System.out.println(provincias[i] + " con " + votosPorProvincia[i] + " votos.");
        }
    }

public static void ordenarPorVotos(int[] votos, String[] provincias) {
    int n = votos.length; // n es la longitud que hay en votosPorProvincia
    for (int i = 0; i < n-1; i++) {
        for (int j = 0; j < n-i-1; j++) {
            if (votos[j] > votos[j+1]) {
                // Intercambiamos votos si el voto j menor que el siguiente
                int tempVotos = votos[j];
                votos[j] = votos[j+1]; 
                votos[j+1] = tempVotos; 
                // Intercambiamos provincias para que este concuerde con la iteracion de los votos
                String tempProvincia = provincias[j];
                provincias[j] = provincias[j+1];
                provincias[j+1] = tempProvincia;
            }
        }
    }
    }

    public static void reporte3() {
        // Se imprime la cantidad de votos de acuerdo con los rangos de edades
        System.out.println("\nCantidad de votos de acuerdo con los rangos de edades:");
        System.out.println("De 18 a 29 anios = " + edadesVotos[0] + " votos.");
        System.out.println("De 30 a 59 anios = " + edadesVotos[1] + " votos.");
        System.out.println("Mayores de 60 = " + edadesVotos[2] + " votos.");
    }
}
